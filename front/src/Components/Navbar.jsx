import { Link, useMatch, useResolvedPath } from "react-router-dom"
import "../Styles/Navbar.css"

const Navbar = ({ setToken }) => {

  const handleLogout = (e) => {
    e.preventDefault();
    localStorage.removeItem('Token')
    setToken(null)
  }


    return (
        <nav className="nav">
          <Link to="/" className="site-title">
            Site Name
          </Link>
          <ul>
            <CustomLink to="/Login">Login</CustomLink>
            <CustomLink to="/SignUP">SignUp</CustomLink>
            <CustomLink to="/Home">Home</CustomLink>
            <CustomLink to="/Profile">Profile</CustomLink>
            <button onClick={handleLogout}>Logout</button>
          </ul>
        </nav>
      )
    }
    
    function CustomLink({ to, children, ...props }) {
      const resolvedPath = useResolvedPath(to)
      const isActive = useMatch({ path: resolvedPath.pathname, end: true })
    
      return (
        <li className={isActive ? "active" : ""}>
          <Link to={to} {...props}>
            {children}
          </Link>
        </li>
      )
    }

export default Navbar