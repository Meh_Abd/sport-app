import { useEffect, useState } from "react"
import Home from "./Components/Home"
import Login from "./Components/Login"
import Navbar from "./Components/Navbar"
import Profile from "./Components/Profile"
import SignUp from "./Components/SignUp"
import { Route, Routes, Navigate } from "react-router-dom"

function App() {
  const [Token, setToken] = useState(null)

  useEffect(() => {
    const storedToken = localStorage.getItem('Token');
    if (storedToken) {
      setToken(storedToken);
    }
  }, [])

  return (
      <>
        <Navbar setToken={setToken} />
          <div className="container">
            <Routes>
              <Route path="/" element={ Token ? ( <Home /> ) : ( <Navigate replace to={ "/Login" } /> ) } />
              <Route path="/Login" element={ !Token ? ( <Login setToken={ setToken }/> ) : ( <Navigate replace to={ "/Home" } /> ) } />
              <Route path="/SignUp" element={ !Token ? ( <SignUp /> ) : ( <Navigate replace to={ "/Home" } /> ) } />
              <Route path="/Home" element={ Token ? ( <Home /> ) : ( <Navigate replace to={ "/Login" } /> ) } />
              <Route path="/Profile" element={ Token ? ( <Profile /> ) : ( <Navigate replace to={ "/Login" } /> ) } />
            </Routes>
        </div>
      </>
  )
}

export default App